package httpserver.itf.impl;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarFile;

import httpserver.itf.HttpRicmlet;

public class Application {
	
	private Map<String, HttpRicmlet> m_ricmlets;
	private ClassLoader              m_parent;
	
	
	public Application(String apppath) throws MalformedURLException {
		ClassLoader curCl  = this.getClass().getClassLoader();
		
		URL         jarUrl = curCl.getResource(apppath);
		
		m_parent   = new URLClassLoader(new URL[] { jarUrl }, curCl);
		m_ricmlets = new HashMap<String, HttpRicmlet>();
	}
	
	public HttpRicmlet getInstance(String clsname, String appname)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Class<?> c = Class.forName((appname + clsname).replace("/", "."), true, m_parent);

		if (m_ricmlets.get(clsname) == null) {
			
			synchronized (c) {
				
				if (m_ricmlets.get(clsname) == null) {
					m_ricmlets.put(clsname, (HttpRicmlet) c.newInstance());
				}
				
			}
			
		}
		
		return m_ricmlets.get(clsname);
	}
}
