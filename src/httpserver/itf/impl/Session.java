package httpserver.itf.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import httpserver.itf.HttpSession;

public class Session implements HttpSession {
	
	public static final long TTL = 60 * 60; // 1h in seconds
	
	private static int ID = 0;
	
	private int                 m_id;
	private long                m_lastRefresh;
	private Map<String, Object> m_data;
	
	
	public Session(int id) {
		m_id          = id;
		m_data        = new HashMap<String, Object>();
		m_lastRefresh = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
	}
	
	public static String newID() {
		return String.valueOf(ID++);
	}
	
	@Override
	public String getId() {
		return String.valueOf(m_id);
	}
	
	@Override
	public Object getValue(String key) {
		return m_data.get(key);
	}
	
	@Override
	public void setValue(String key, Object value) {
		m_data.put(key, value);
	}
	
	@Override
	public void refresh() {
		m_lastRefresh = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
	}
	
	@Override
	public boolean canBeDelete() {
		long curSec = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
		return (curSec - m_lastRefresh) >= TTL;
	}
}
