package httpserver.itf.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import httpserver.itf.HttpRequest;
import httpserver.itf.HttpResponse;
import httpserver.itf.HttpRicmlet;
import httpserver.itf.HttpSession;

/**
 * Basic Http Server Implementation
 * 
 * Only manages static requests The url for a static ressource is of the form:
 * "http//host:port/<path>/<ressource name>" For example, try accessing the
 * following urls from your brower, knowing that the current Eclipse project
 * contains a FILES directory: http://localhost:<port>/FILES/
 * http://localhost:<port>/FILES/voile.jpg ...
 */
public class HttpServer {
	
	private int          m_port;
	private File         m_folder;
	private ServerSocket m_ssoc;
	
	private static Map<String, Application> m_apps;
	private static Map<String, HttpRicmlet> m_ricmlets;
	private static Map<String, HttpSession> m_sessions;
	
	
	protected HttpServer(int port, String folderName) {
		m_apps     = new HashMap<String, Application>();
		m_ricmlets = new HashMap<String, HttpRicmlet>();
		m_sessions = new HashMap<String, HttpSession>();
		
		m_port = port;
		
		if (!folderName.endsWith(File.separator))
			folderName = folderName + File.separator;
		m_folder = new File(folderName);
		
		try {
			m_ssoc = new ServerSocket(m_port);
			System.out.println("HttpServer started on port " + m_port);
		} catch (IOException e) {
			System.out.println("HttpServer Exception:" + e);
			System.exit(1);
		}
		
	}
	
	public File getFolder() {
		return m_folder;
	}
	
	public HttpRicmlet getInstance(String clsname, String appname)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, MalformedURLException {
		
		if (getClass().getClassLoader().getResource(appname + ".jar") == null) {
			String packageName = appname + '/' + clsname;
			packageName = packageName.replace("/", ".");
			
			return getRicmlet(packageName);
		}
		
		return getRicmletFromApp(clsname, appname);
	}
	
	private HttpRicmlet getRicmlet(String clsname)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> c = Class.forName(clsname);
		
		// We create a new instance if the instance is null or if the class instance is
		// different of the servlet class requested
		if (m_ricmlets.get(clsname) == null) {
			
			// To prevent multiple instanciation by different threads
			synchronized (c) {
				
				if (m_ricmlets.get(clsname) == null) {
					m_ricmlets.put(clsname, (HttpRicmlet) c.newInstance());
				}
				
			}
			
		}
		
		return m_ricmlets.get(clsname);
	}
	
	private HttpRicmlet getRicmletFromApp(String clsname, String appname)
			throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		if (m_apps.get(appname) == null) {
			
			synchronized (Application.class) {
				
				if (m_apps.get(appname) == null) {
					m_apps.put(appname, new Application(appname + ".jar"));
				}
				
			}
			
		}
		
		return m_apps.get(appname).getInstance(clsname, appname + "/");
	}
	
	protected void loop() {
		
		try {
			
			while (true) {
				Socket soc = m_ssoc.accept();
				(new HttpWorker(this, soc)).start();
				
				synchronized (m_sessions) {
					
					for (Map.Entry<String, HttpSession> e : m_sessions.entrySet()) {
						
						if (e.getValue().canBeDelete()) {
							m_sessions.remove(e.getKey());
							System.out.println("Session " + e.getKey() + " deleted");
						}
						
					}
					
				}
				
			}
			
		} catch (IOException e) {
			System.out.println("HttpServer Exception, skipping request");
			e.printStackTrace();
		}
		
	}
	
	/*
	 * Reads a request on the given input stream and returns the corresponding
	 * HttpRequestImpl object
	 */
	public HttpRequest getRequest(BufferedReader br) throws IOException {
		String      line    = null;
		HttpRequest request = null;
		line = br.readLine();
		StringTokenizer parse    = new StringTokenizer(line);
		String          method   = parse.nextToken().toUpperCase();
		String          ressname = parse.nextToken();
		
		if (method.equals("GET")) {
			if (ressname.contains("ricmlet"))
				request = new HttpRicmletRequestImpl(this, method, ressname, br);
			else
				request = new HttpStaticRequest(this, method, ressname);
		} else
			request = new UnknownRequest(this, method, ressname);
		
		return request;
	}
	
	/*
	 * Returns an HttpResponse object corresponding the the given HttpRequest object
	 */
	public HttpResponse getResponse(HttpRequest req, PrintStream ps) {
		return new HttpRicmletResponseImpl(this, req, ps);
	}
	
	public HttpSession getSession(String id) {
		
		if (m_sessions.get(id) == null) {
			
			synchronized (m_sessions) {
				
				if (m_sessions.get(id) == null) {
					m_sessions.put(id, new Session(Integer.valueOf(id)));
				}
				
			}
			
		}
		
		return m_sessions.get(id);
	}
	
	public static void main(String[] args) {
		int port = 0;
		
		if (args.length != 2) {
			System.out.println("Usage: java Server <port-number> <file folder>");
		} else {
			port = Integer.parseInt(args[0]);
			String     foldername = args[1];
			HttpServer hs         = new HttpServer(port, foldername);
			hs.loop();
		}
		
	}
	
}
