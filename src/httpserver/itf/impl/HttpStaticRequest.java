package httpserver.itf.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import httpserver.itf.HttpRequest;
import httpserver.itf.HttpResponse;

public class HttpStaticRequest extends HttpRequest {
	
	static final int    BUF_SZ       = 1024;
	static final String DEFAULT_FILE = "index.html";
	
	
	public HttpStaticRequest(HttpServer hs, String method, String ressname) throws IOException {
		super(hs, method, ressname);
	}
	
	public void process(HttpResponse resp) throws Exception {
		String folder = m_hs.getFolder().getName();
		m_ressname = folder + m_ressname;
		File f = new File(m_ressname);
		
		if (f.exists()) {
			
			if (f.isDirectory()) {
				m_ressname += "/" + DEFAULT_FILE;
				f           = new File(m_ressname);
			}
			
			resp.setReplyOk();
			resp.setContentType(getContentType(m_ressname));
			resp.setContentLength((int) f.length());
			
			byte            buff[] = new byte[(int) f.length()];
			PrintStream     ps     = resp.beginBody();
			FileInputStream fis    = new FileInputStream(f);
			
			fis.read(buff);
			fis.close();
			
			ps.write(buff);
			ps.flush();
			
		} else {
			resp.setReplyError(404, "Not found");
		}
		
	}
}
