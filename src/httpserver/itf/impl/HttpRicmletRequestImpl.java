package httpserver.itf.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import httpserver.itf.HttpResponse;
import httpserver.itf.HttpRicmletRequest;
import httpserver.itf.HttpRicmletResponse;
import httpserver.itf.HttpSession;

public class HttpRicmletRequestImpl extends HttpRicmletRequest {
	
	private Map<String, String> m_args;
	private Map<String, String> m_cookies;
	private HttpSession         m_session;
	
	
	public HttpRicmletRequestImpl(HttpServer hs, String method, String ressname, BufferedReader br) throws IOException {
		super(hs, method, ressname);
		
		m_args    = new HashMap<String, String>();
		m_cookies = new HashMap<String, String>();
		
		String line = null;
		
		while (!(line = br.readLine()).isEmpty()) {
			
			if (line.startsWith("Cookie")) {
				
				StringTokenizer parse = new StringTokenizer(line);
				parse.nextToken();
				
				while (parse.hasMoreTokens()) {
					String   tmpCookie = parse.nextToken(";");
					String[] val       = tmpCookie.split("=");
					val[0] = val[0].substring(1);
					m_cookies.put(val[0], val[1]);
				}
				
			}
			
		}
		
		if (!m_cookies.containsKey("session_id")) {
			m_session = m_hs.getSession(Session.newID());
			m_cookies.put("session_id", m_session.getId());
		} else {
			m_session = m_hs.getSession(m_cookies.get("session_id"));
			m_session.refresh();
		}
		
	}
	
	@Override
	public void process(HttpResponse resp) throws Exception {
		
		String ressource = m_ressname.substring(1);
		
		if (ressource.contains("?")) { // Case when there are arguments
			ressource = ressource.substring(0, m_ressname.indexOf("?") - 1);
			
			// We get the GET arguments
			String sargs[] = m_ressname.substring(m_ressname.indexOf("?") + 1).split("&");
			
			for (String a : sargs) {
				int i = a.indexOf("=");
				m_args.put(a.substring(0, i), a.substring(i + 1));
			}
			
		}
		
		String apppath = ressource.substring(0, ressource.lastIndexOf("/"));
		String clsname = ressource.substring(ressource.lastIndexOf("/") + 1);
		
		try {
			if (!ressource.contains("favicon.ico"))
				m_hs.getInstance(clsname, apppath).doGet(this, (HttpRicmletResponse) resp);
		} catch (ClassNotFoundException e) {
			resp.setReplyError(404, "Ricmlet not found");
		}
		
	}
	
	@Override
	public HttpSession getSession() {
		return m_session;
	}
	
	@Override
	public String getArg(String name) {
		return m_args.get(name);
	}
	
	@Override
	public String getCookie(String name) {
		return m_cookies.get(name);
	}
	
	public void setCookie(String name, String val) {
		m_cookies.put(name, val);
	}
}
