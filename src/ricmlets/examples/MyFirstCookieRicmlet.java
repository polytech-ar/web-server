package ricmlets.examples;

import java.io.IOException;
import java.io.PrintStream;

import httpserver.itf.HttpRicmletRequest;
import httpserver.itf.HttpRicmletResponse;

public class MyFirstCookieRicmlet implements httpserver.itf.HttpRicmlet {
	
	@Override
	public void doGet(HttpRicmletRequest req, HttpRicmletResponse resp) throws IOException {
		resp.setReplyOk();
		resp.setContentType("text/html");
		
		String name     = "MyFirstCookie";
		int    myCookie = 0;
		
		if (req.getCookie(name) == null) {
			System.out.println("Cookie not defined");
			req.setCookie(name, String.valueOf(myCookie));
			resp.setCookie(name, String.valueOf(myCookie));
		} else {
			try {
				myCookie = Integer.parseInt(req.getCookie(name));
			}catch(NumberFormatException e) {
				
			}
		}
		
		myCookie++;
		req.setCookie(name, String.valueOf(myCookie));
		resp.setCookie(name, String.valueOf(myCookie));
		
		PrintStream ps = resp.beginBody();
		ps.println("<HTML><HEAD><TITLE> Ricmlet processing </TITLE></HEAD>");
		ps.print("<BODY><H4> Hello " + String.valueOf(myCookie) + " !!!");
		ps.println("</H4></BODY></HTML>");
		ps.println();
	}
	
}
